import { Col, Button, Row, Container, Card, Form } from 'react-bootstrap';
import { useState, useEffect, useContext } from "react";
import { useNavigate, Navigate, useParams } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Login(props) {
  const { user, setUser } = useContext(UserContext);
  // const {userId} = useParams();

  const navigate = useNavigate();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [isActive, setIsActive] = useState(false);

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {

        // Global user state for validation across the whole project
        // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application.
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };

  function authenticate(e) {
    e.preventDefault();
    
    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {

        if (typeof data.access !== "undefined") {
          localStorage.setItem("token", data.access);
          retrieveUserDetails(data.access);

          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome to E-Shop!",
          });
        } else {
          Swal.fire({
            title: "Authentication Failed",
            icon: "error",
            text: "Please check your login details and try again!",
          });
        }
      });

    setEmail("");
    setPassword("");
  }

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return user.id !== null ? (
    <Navigate to="/products" />
  ) : (
  <div>
      <Container>
        <Row className="vh-100 d-flex justify-content-center align-items-center">
          <Col md={8} lg={6} xs={12}>
            <Card className="px-4">
              <Card.Body>
                <div className="mb-3 mt-md-4">
                  <h2 className="fw-bold mb-2 text-center text-uppercase ">
                    Login
                  </h2>
                  <div className="mb-3">
                    <Form onSubmit={(e) => authenticate(e)}>
                      <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label className="text-center">
                          Email address
                        </Form.Label>
                          <Form.Control
                            type="email"
                            placeholder="Enter email here"
                            onChange={(e) => setEmail(e.target.value)}
                            value={email}
                            required
                          />
                      </Form.Group>

                      <Form.Group
                        className="mb-3"
                        controlId="formBasicPassword"
                      >
                        <Form.Label>Password</Form.Label>
                          <Form.Control
                            type="password"
                            placeholder="Enter password here"
                            onChange={(e) => setPassword(e.target.value)}
                            value={password}
                            required
                          />
                      </Form.Group>
                      <Form.Group
                        className="mb-3"
                        controlId="formBasicCheckbox"
                      ></Form.Group>
                      <div className="d-grid">
                        {
                          (isActive)?(
                            <Button variant="danger" type="submit">
                              Login
                            </Button>
                          ):(
                            <Button variant="danger" type="submit" disabled>
                              Login
                            </Button>
                          )
                        }
                      </div>
                    </Form>
                    <div className="mt-3">
                      <p className="mb-0  text-center">
                        Don't have an account yet??{' '}
                        <a href="/register" className="text-danger fw-bold">
                          SignIn
                        </a>
                      </p>
                    </div>
                  </div>
                </div>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
}