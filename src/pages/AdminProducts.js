import AdminProductCard from "../components/AdminProductCard";
import { useState, useEffect } from "react";

export default function AdminProducts() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then((res) => res.json())
      .then((data) => {
        setProducts(
          data.map((product) => {
            return <AdminProductCard key={product._id} product={product} />;
          })
        );
      });
  }, []);

  return <>{products}</>;
}
