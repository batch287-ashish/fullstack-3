import ProductCard from "../components/ProductCard";
import { useState, useEffect } from "react";
import { Row} from 'react-bootstrap';

export default function Products() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/active-products`)
      .then((res) => res.json())
      .then((data) => {
        setProducts(
          data.map((product) => {
            return <ProductCard key={product._id} product={product} />;
          })
        );
      });
  }, []);

  return (
    <Row className="mt-3 mb-3">
      {products}
    </Row>
  );
}
