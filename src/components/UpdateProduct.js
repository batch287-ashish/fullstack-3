import {useState, useContext, useEffect} from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import {useParams, Link, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function UpdateProduct() {


	const navigate = useNavigate();

	const {productId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const update = (event, productId) => {
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: "PATCH",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				price: price,
				description: description
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				Swal.fire({
					title: "Successfully updated",
					icon: "success",
					text: "You have successfully updated the product."
				})

				navigate("/admin")

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}

		})
	};

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId]);

	return (

		<Container>
			<Row>
				<Col lg={{span: 6, offset:3}} >
					<Card>
				      <Card.Body className="text-center">
				      	<Form onSubmit={(event) => update(event, productId)}>
	                      <Form.Group className="mb-3" controlId="form.Name">
	                        <Form.Label className="text-center">
	                          Name of the product
	                        </Form.Label>
	                          <Form.Control
	                            type="text"
	                            placeholder="Product Name"
	                            onChange={(e) => setName(e.target.value)}
	                            value={name}
	                            required
	                          />
	                      </Form.Group>
		                      <Form.Group className="mb-3" controlId="form.Number">
	                        <Form.Label className="text-center">
	                          Price
	                        </Form.Label>
	                          <Form.Control
	                            type="number"
	                            placeholder="$ Price of product"
	                            onChange={(e) => setPrice(e.target.value)}
	                            value={price}
	                            required
	                          />
	                      </Form.Group>
	                      <Form.Group className="mb-3" controlId="form.Textarea">
	                        <Form.Label className="text-center">
	                          Description
	                        </Form.Label>
	                          <Form.Control
	                            as="textarea" rows={3}
	                            placeholder="Describe about product"
	                            onChange={(e) => setDescription(e.target.value)}
	                            value={description}
	                            required
	                          />
	                      </Form.Group>
			
		        		  <Button variant="danger" type="submit">Update</Button>
		                </Form>
				      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}
