import { Card, Button, Col, Row } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

export default function ProductCard({product}) {

const { name, description, price, _id } = product;

	return (
		<Col className='my-2' xs={12} md={6}>
			<Card className="cardHighlight p-0">
				<Card.Body>
					<Card.Title><h4>{name}</h4></Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>$ {price}</Card.Text>
					<Button className="btn btn-danger" as={Link} to={`/products/${_id}`}>Details</Button>
				</Card.Body>
			</Card>
		</Col>
	)
}
